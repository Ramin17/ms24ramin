package az.ramin.ms24.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ms24")
public class HelloApi {


    @GetMapping("hello1")
    public String sayhello1(){
        return "Hello World 1";
    }

    @GetMapping("hello2")
    public String sayhello2(){
        return "Hello World 2";
    }
}
